

## Some Common Rendering Differences between RedCarpet and CommonMark

### Tight Lists

- first item

  this is a paragraph

- second item

### List Indentation

**RedCarpet**

1. There are few rules that we need to disable due to technical debt. Which are:
  1. [no-new][eslint-new]
  1. [class-methods-use-this][eslint-this]

**CommonMark**

1. There are few rules that we need to disable due to technical debt. Which are:
   1. [no-new][eslint-new]
   1. [class-methods-use-this][eslint-this]


### Blockquotes

**RedCarpet**

>
This paragraph is blockquoted in Redcarpet, but not in CommonMark

**CommonMark**

> This paragraph is blockquoted in Redcarpet __and__ CommonMark.

### YAML frontmatter

Both Redcarpet and CommonMark don't process YAML frontmatter in a meaningful way

### Link definitions

**RedCarpet**

[link title]
(http://example.com)

**CommonMark**

[link title](http://example.com)
