## Generate Differences Between RedCarpet and CommonMark

The RedCarpet variant of Markdown is different than the CommonMark variant.
Moving from one to the other can cause your content to be rendered differently.

This project attempts to highlight the differences in your files between the two,
so that it's easier to see what files might need to be changed to fix the rendering
inconsistencies.

You can also use it to convert from RedCarpet or kramdown into CommonMark.

## Installation

First make sure that [Pandoc is installed](http://johnmacfarlane.net/pandoc/installing.html).

Then install the necessary gems:

```
bundle install
```

## Usage

```
Generates differences and/or converts between processing Markdown files with RedCarpet and CommonMark

Usage: diff_mark.rb [options] [file or directory]
        --convert                    Convert to CommonMark in-place, overwriting existing files
        --html                       Output rendered HTML of differing documents
        --kramdown                   Process Markdown with kramdown instead of RedCarpet
    -v, --verbose                    Lists successes and failures
    -h, --help                       Show this help
```

To run it against a directory of files,

```
./diff_mark.rb path_to_directory_of_files
```

This will output a list of the `.md` files that have differences.  Adding the `--html` flag will write out the conversion files so that you can see what's different.

```
./diff_mark.rb --html path_to_directory_of_files
```

The differences will be saved in the `diffs` directory.

You can also compare kramdown and CommonMark.  Just add the `--kramdown` option.

Use the `--convert` flag to convert the RedCarpet/kramdown markdown into CommonMark and save the updated files
in the original location, overwriting the original version.  This is great if you have a repository
of markdown files you want to convert.  Run this on the directory, review your changes and then commmit
them.

```
./diff_mark.rb --convert path_to_directory_of_files
```

## Method of Comparing

RedCarpet and CommonMark generate very different HTML - one escapes entities and the other doesn't, spacing can be handled differently, etc.  So raw comparing of generated HTML is not practical.  Instead, we use Pandoc to take the HTML and convert into a common format - CommonMark.  Pandoc is very good at moving between formats.  In this way, we can get a much more relevant comparison, with most of the inconsistencies removed.
